package akkaExample

import akka.http.scaladsl.server.Route

trait RouteGenerator {
    def getRoutes(): Route
}
