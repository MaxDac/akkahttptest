package akkaExample

import akka.Done
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akkaExample.Main._
import spray.json.DefaultJsonProtocol._

import scala.concurrent.Future

object OrderWebServer extends RouteGenerator {

    // domain model
    final case class Item(name: String, id: Long)
    final case class Order(items: List[Item])

    var orders: List[Item] = Nil;

    // json formatting
    implicit val itemFormat = jsonFormat2(Item)
    implicit val orderFormat = jsonFormat1(Order)

    def fetchItem(id: Long): Future[Option[Item]] = Future {
        orders.find(o => o.id == id)
    }

    def saveOrder(order: Order): Future[Done] = {
        orders = order match {
            case Order(items) => items ::: orders
            case _ => orders
        }
        Future { Done }
    }

    def getRoutes(): Route = {
        get {
            pathPrefix("item" / LongNumber) { id =>
                val maybeItem: Future[Option[Item]] = fetchItem(id)

                onSuccess(maybeItem) {
                    case Some(item) => complete(item)
                    case None => complete(StatusCodes.NotFound)
                }
            }
        } ~
        post {
            path("create-order") {
                entity(as[Order]) { order =>
                    val saved: Future[Done] = saveOrder(order)

                    onComplete(saved) { done => complete("order created") }
                }
            }
        }
    }
}
