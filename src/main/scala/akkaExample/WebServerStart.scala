package akkaExample

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

object WebServerStart extends RouteGenerator {

    def getRoutes(): Route = {
        path("pitu-bella") {
            get {
                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Pitu is really beautiful!!</h1>"))
            }
        }
    }
}
