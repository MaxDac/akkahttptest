package akkaExample

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scala.io.StdIn

object Main {
    implicit val system = ActorSystem("test-system")
    implicit val materializer = ActorMaterializer()

    val serviceName = "Credito"
    val hostname = "amqp://Credito:Credito@localhost:5672/Credito"

    // Needed for the execution of the futures
    implicit val executionContext = system.dispatcher

    def main(args: Array[String]): Unit = {

//        val route = path("pitu-bella") {
//            get {
//                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Pitu is really beautiful!!</h1>"))
//            }
//        }
        val hostname = "localhost"
        val port = 8092
        val http = Http()
//        val bindingFuture = http.bindAndHandle(route, hostname, port)

        val routes: Route = WebServerStart.getRoutes() ~
            OrderWebServer.getRoutes() ~
            RandomServiceRouteGenerator.getRoutes() ~
            BidderRouteGenerator.getRoutes()

        val binding = http.bindAndHandle(routes, hostname, port)
        println(s"Service is ready at $hostname:$port")
        StdIn.readLine()

        binding
            .flatMap(_.unbind())
            .onComplete(_ => system.terminate())
    }
}
