package akkaExample

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import akkaExample.Main._
import spray.json.DefaultJsonProtocol._

import scala.concurrent.Future
import scala.concurrent.duration._

object BidderRouteGenerator extends RouteGenerator {
    case class Bid(userId: String, offer: Int)
    case object GetBids
    case class Bids(bids: List[Bid])

    class Auction extends Actor with ActorLogging {
        var bids = List.empty[Bid]

        override def receive: Receive = {
            case bid @ Bid(userid, offer) => {
                bids = bids :+ bid
                log.info(s"Bid complete: $userid, $offer")
            }
            case GetBids => sender() ! Bids(bids)
            case _ => log.info("Invalid message")
        }
    }

    // Json formatting
    implicit val bidFormat = jsonFormat2(Bid)
    implicit val bidsFormat = jsonFormat1(Bids)

    // Actor
    val auction = system.actorOf(Props[Auction], "auction")

    override def getRoutes(): Route = {
        path("auction") {
            post {
                parameter("bid".as[Int], "user") {
                    (bid, user) => {
                        auction ! Bid(user, bid)
                        complete((StatusCodes.Accepted, "bid placed"))
                    }
                }
            } ~
            get {
                implicit val timeout: Timeout = 5.seconds

                // query the actor for the current auction state
                val bids: Future[Bids] = (auction ? GetBids).mapTo[Bids]
                complete(bids)
            }
        }
    }
}
