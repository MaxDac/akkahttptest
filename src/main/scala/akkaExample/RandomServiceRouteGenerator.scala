package akkaExample

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.scaladsl.Source
import akka.util.ByteString

import scala.util.Random

object RandomServiceRouteGenerator extends RouteGenerator {
    override def getRoutes(): Route = {
        val numbers = Source.fromIterator(() => Iterator.continually(Random.nextInt()))

        path("random") {
            get {
                complete(
                    HttpEntity(ContentTypes.`text/plain(UTF-8)`, numbers.map(n => ByteString(s"$n\n")))
                )
            }
        }
    }
}
