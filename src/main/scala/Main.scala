import org.scalatest._
import akka.http.scaladsl.testkit._
import akka.http.scaladsl.model._
import akka.util.ByteString

class RestSpec extends WordSpec with Matchers with ScalatestRouteTest with RestService {
    val jsonRequest = ByteString(
        s"""
           |{
           |    "name": "test"
           |}l
         """.stripMargin)

    val getRequest = HttpRequest(
        HttpMethods.GET,
        uri = "/pitu-bella",
        entity = HttpEntity(MediaTypes.`application/json`, jsonRequest)
    )

    getRequest ~> route ~> check {

    }
}
