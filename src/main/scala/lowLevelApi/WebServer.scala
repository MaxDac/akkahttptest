package lowLevelApi

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.GET
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer

import scala.io.StdIn

object WebServer {

    def main(args: Array[String]): Unit = {
        implicit val system = ActorSystem()
        implicit val materializer = ActorMaterializer()
        implicit val executionContext = system.dispatcher

        val requestHandler: HttpRequest => HttpResponse = {
            case HttpRequest(GET, Uri.Path("/"), _, _, _) =>
                HttpResponse(entity = HttpEntity(
                    ContentTypes.`text/html(UTF-8)`,
                    "<html><body>Pitu bella!</body></html>"))
            case HttpRequest(GET, Uri.Path("/ping"), _, _, _) => HttpResponse(entity = "PONG!")
            case HttpRequest(GET, Uri.Path("/crash"), _, _, _) => sys.error("Padulo!")
            case r : HttpRequest => {
                // Important to drain incoming HTTP Entity stream
                r.discardEntityBytes()
                HttpResponse(404, entity = "Unknown Padulo")
            }
        }

        var bindingFuture = Http().bindAndHandleSync(requestHandler, "localhost", 8093)
        println(s"Server online at http://localhost:8093/ \nPress return to close.")
        StdIn.readLine()
        bindingFuture
            .flatMap(_.unbind())
            .onComplete(_ => system.terminate())
    }

}
