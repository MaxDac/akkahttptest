
name := "AkkaHttpTest"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-http-spray-json"   % "10.1.1",
    "com.typesafe.akka" %% "akka-http"   % "10.1.1",
    "com.typesafe.akka" %% "akka-stream" % "2.5.11",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1" % Test,
    "org.scalatest" %% "scalatest" % "3.2.0-SNAP10" % Test,
    "com.github.swagger-akka-http" %% "swagger-akka-http" % "0.14.0"
)